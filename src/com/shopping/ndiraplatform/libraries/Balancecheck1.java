package com.shopping.ndiraplatform.libraries;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.shopping.ndiraplatform.utils.Utils;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;


public class Balancecheck1 extends shopping_ndiraplatform_Constants
{
	public boolean balancecheck() throws IOException 
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Select PaymentSelection=new Select(d.findElement(By.xpath(".//*[@id='repeatSelect']")));
		PaymentSelection.selectByVisibleText("New Direction IRA Account");
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/button")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		Login li=new Login();
		li.login();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Select account=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/form[2]/select")));
		account.selectByIndex(1);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			
		String CashAllocated=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/form[2]/table/tfoot/tr/th[2]/input")).getAttribute("value");
		String CashAvailable=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/form[2]/table/tfoot/tr/th[3]/input")).getAttribute("value");
		double total=Utils.convertCurrencyFormateToDouble(CashAllocated, Locale.US)+Utils.convertCurrencyFormateToDouble(CashAvailable, Locale.US);
		String PurchaseAmount=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/form[2]/table/tfoot/tr/th[4]/input")).getAttribute("value");
		Sleeper.sleepTightInSeconds(2);
		if (total<Utils.convertCurrencyFormateToDouble(PurchaseAmount, Locale.US))
		{		
			Sleeper.sleepTightInSeconds(2);
			//String acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/h3[1]/b")).getText();
			//String acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/div/div/p[1]/b")).getText();
			String acmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]")).getText();
			
			String expmsg="Insufficient";
			
			
			if (acmsg.contains(expmsg))
			{
				return true;
			} 
			else 
			{
				return false;
			}
								
		}
		else
		{
			return false;
		}
		
		} 
		catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	  }
}
