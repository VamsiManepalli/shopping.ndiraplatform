package com.shopping.ndiraplatform.libraries;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;

public class Remove_All extends shopping_ndiraplatform_Constants
{
	public static boolean Removeall() 
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[2]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[3]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[4]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[5]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[6]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[3]")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
		String expmsg,acmsg;
		expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).getText();
		acmsg="Items In Cart: 0 || Checkout Price: $0.00";
		//System.out.println(expmsg);
		if (expmsg.equalsIgnoreCase(acmsg))
		{
			return true;	
		} 
		else 
		{
			return false;
		}

		
		} catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
