package com.shopping.ndiraplatform.libraries;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;

public class CreditCard extends shopping_ndiraplatform_Constants
{
	public static String name,number,ExpirationDate,CVV,Street,City,State,ZipCode;
	public static boolean creditcard() 
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Select PaymentSelection=new Select(d.findElement(By.xpath(".//*[@id='repeatSelect']")));
		PaymentSelection.selectByVisibleText("Credit Card");
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/button")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/input[1]")).sendKeys(name);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/input[2]")).sendKeys(number);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/input[3]")).sendKeys(ExpirationDate);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/input[4]")).sendKeys(CVV);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/input[5]")).click();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/input[1]")).sendKeys(Street);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/input[2]")).sendKeys(City);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/input[3]")).sendKeys(State);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div/input[4]")).sendKeys(ZipCode);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Sleeper.sleepTightInSeconds(4);
		String expmsg,acmsg;
		expmsg=d.findElement(By.xpath("html/body/div[1]/div/div[2]/h2/b")).getText();
		acmsg="Checkout Complete. Thanks for Shopping!";
		if (expmsg.equalsIgnoreCase(acmsg))
		{
			return true;
		} 
		else 
		{
			return false;
		}

		
		} catch (Exception e) 
		{
			System.out.println(e);
			return false;
		}
		
	}
}
