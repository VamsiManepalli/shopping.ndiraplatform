package com.shopping.ndiraplatform.libraries;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.openqa.selenium.support.ui.Select;

import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;

public class CreateAccount extends shopping_ndiraplatform_Constants
{
	public static boolean createaccount() 
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[1]")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		Sleeper.sleepTightInSeconds(5);
		Select PaymentSelection=new Select(d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/select")));
		PaymentSelection.selectByVisibleText("New Direction IRA Account");
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/div[1]/button")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String expurl,acurl;
		expurl=d.getCurrentUrl();
		d.findElement(By.xpath("html/body/div[1]/form/p/span")).click();
		acurl=d.getCurrentUrl();
		if (!expurl.equalsIgnoreCase(acurl))
		{
			return false;
		} 
		else
		{
			return true;
		}

		
		} catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
