package com.shopping.ndiraplatform.libraries;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;

public class Continue_Shopping extends shopping_ndiraplatform_Constants
{
	public static boolean Continue_Shopping() 
	{
		try 
		{
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String item1,item2;
		item1=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[1]/p/b")).getText();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[1]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String cartitem1=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/table/tfoot/tr[1]/th[1]/input")).getAttribute("value");
		if (item1.equalsIgnoreCase(cartitem1)) 
		{
			d.findElement(By.xpath("html/body/div[1]/div/div[2]/button[2]")).click();
		}
		else 
		{
			d.get(url);
			return false;
		}
		Sleeper.sleepTightInSeconds(2);
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		item2=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[2]/div/div/div[1]/p/b")).getText();
		d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/div/div[2]/div/div/div[2]/button")).click();
		d.switchTo().alert().accept();
		d.findElement(By.xpath("html/body/div[1]/div/div[1]/header/div/div/div[2]/ul[2]/li/a")).click();
		d.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String cartitem2=d.findElement(By.xpath("html/body/div[1]/div/div[2]/form/table/tfoot/tr[2]/th[1]/input")).getAttribute("value");
		if (item2.equalsIgnoreCase(cartitem2)) 
		{
			return true;
		}
		else 
		{
			return false;
		}

		
		} catch (Exception e)
		{
			System.out.println(e);
			return false;
		}
	}
}
