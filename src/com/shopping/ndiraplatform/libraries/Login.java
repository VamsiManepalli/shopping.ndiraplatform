package com.shopping.ndiraplatform.libraries;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import com.shopping.ndiraplatform.utils.XLUtils;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Utils;

public class Login extends shopping_ndiraplatform_Constants
{
	
	public void login() throws IOException
	{
		
		XLUtils xl = new XLUtils();
		String xlfile = shopping_ndiraplatform_Utils.getProperty("file.xlfile.path");
		String tcsheet = "TestCases";
		String tssheet = "TestSteps";
		
		//From Properties File
		//String uname=shopping_ndiraplatform_Utils.getProperty("username");
		//String password=shopping_ndiraplatform_Utils.getProperty("password");
		
		//From Excel
		//String uname=xl.getCellData(xlfile, tssheet, 1, 5);
		//String password=xl.getCellData(xlfile, tssheet, 1, 6);
		
			
		d.findElement(By.id("input-login")).sendKeys(InputParameters.username);
		d.findElement(By.id("input-password")).sendKeys(InputParameters.password);
		Sleeper.sleepTightInSeconds(2);
		d.findElement(By.xpath(".//*[@id='login-view']/form/div[3]/button")).click();
	}
}
