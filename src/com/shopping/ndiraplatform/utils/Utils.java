package com.shopping.ndiraplatform.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Utils {
	
	public static double convertCurrencyFormateToDouble(String valueString, Locale locale) {
		double amount = 0;
		NumberFormat cf = NumberFormat.getCurrencyInstance(locale);
		try {
			Number number = cf.parse(valueString);
			amount = number.doubleValue();
		} catch (ParseException e) {
			amount = 0;
			e.printStackTrace();
		}
		return amount;
	}
}
