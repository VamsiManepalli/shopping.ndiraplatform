package com.shopping.ndiraplatform.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.server.browserlaunchers.Sleeper;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

	public class shopping_ndiraplatform_Constants
	{
	  public static WebDriver d=new FirefoxDriver();
	  public static String url=shopping_ndiraplatform_Utils.getProperty("url");
	  
	  @BeforeTest
	  public void Launch()
	  {
		  Sleeper.sleepTightInSeconds(2);
		  d.get(url);
		  Sleeper.sleepTightInSeconds(2);
		  d.manage().window().maximize();
	  }
	 @AfterTest
	  public void Close()
	  {
		  d.close();
	  }
	
	}


