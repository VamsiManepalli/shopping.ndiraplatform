package com.shopping.ndiraplatform.tests;

import java.io.IOException;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.shopping.ndiraplatform.libraries.Balancecheck1;
import com.shopping.ndiraplatform.libraries.Balancecheck2;
import com.shopping.ndiraplatform.libraries.Balancecheck3;
import com.shopping.ndiraplatform.libraries.Continue_Shopping;
import com.shopping.ndiraplatform.libraries.CreateAccount;
import com.shopping.ndiraplatform.libraries.CreditCard;
import com.shopping.ndiraplatform.libraries.InputParameters;
import com.shopping.ndiraplatform.libraries.Remove_All;
import com.shopping.ndiraplatform.libraries.SponsorName;
import com.shopping.ndiraplatform.utils.XLUtils;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Constants;
import com.shopping.ndiraplatform.utils.shopping_ndiraplatform_Utils;

public class shopping_ndiraplatform_Test extends shopping_ndiraplatform_Constants
{
	XLUtils xl = new XLUtils();
	String xlfile = shopping_ndiraplatform_Utils.getProperty("file.xlfile.path");
	String tcsheet = "TestCases";
	String tssheet = "TestSteps";
	int tccount, tscount;
	String tcexe;
	String tcid, tsid, keyword;
	String tcres = "";
	boolean res = false;
	
	SponsorName name=new SponsorName();
	Balancecheck1 bc1=new Balancecheck1();
	Balancecheck2 bc2=new Balancecheck2();
	Balancecheck3 bc3=new Balancecheck3();
	Remove_All ra=new Remove_All();
	Continue_Shopping cs=new Continue_Shopping();
	CreditCard cd=new CreditCard();
	CreateAccount ca=new CreateAccount();
	
	@Parameters({ "username", "password", "clientname" })
	@Test
	public void shoppingcartTest(String username, String password, String clientname ) throws IOException 
	{	
		saveInputParameters(username, password, clientname);
		
		tccount = XLUtils.getRowCount(xlfile, tcsheet);
		tscount = XLUtils.getRowCount(xlfile, tssheet);
		for (int i = 1; i <= tccount; i++)
		{
			tcexe = xl.getCellData(xlfile, tcsheet, i, 2);
			if (tcexe.equalsIgnoreCase("Y"))
			{
				tcid = xl.getCellData(xlfile, tcsheet, i, 0);
				for (int j = 1; j <= tscount; j++) 
				{
					try
					{
						tsid = xl.getCellData(xlfile, tssheet, j, 0);
					} 
					catch (Exception e) 
					{
						System.out.println("");
					}
					if (tcid.equalsIgnoreCase(tsid)) 
					{
						keyword = xl.getCellData(xlfile, tssheet, j, 4);

						switch (keyword.toUpperCase()) 
						{
						
						case "NAME":
							SponsorName.epname = xl.getCellData(xlfile, tssheet, j, 5);
							res = name.sample1();
							d.get(url);
							break;
						
						case "BALANCECHECK1":
							res = bc1.balancecheck();
							d.get(url);							
							break;
							
						case "BALANCECHECK2":
							res = bc2.balancecheck();
							d.get(url);
							break;
							
						case "BALANCECHECK3":
							res = bc3.balancecheck();
							d.get(url);
							
							break;
							
						case "REMOVEALL":
							res = ra.Removeall();
							break;
							
						case "CONTINUESHOPPING":
							res = cs.Continue_Shopping();
							d.get(url);
							break;
							
						case "CREDITCARD":
							cd.name=xl.getCellData(xlfile, tssheet, j, 5);
							cd.number=xl.getCellData(xlfile, tssheet, j, 6);
							cd.ExpirationDate=xl.getCellData(xlfile, tssheet, j, 7);
							cd.CVV=xl.getCellData(xlfile, tssheet, j, 8);
							cd.Street=xl.getCellData(xlfile, tssheet, j, 9);
							cd.City=xl.getCellData(xlfile, tssheet, j, 10);
							cd.State=xl.getCellData(xlfile, tssheet, j, 11);
							cd.ZipCode=xl.getCellData(xlfile, tssheet, j, 12);
							res = cd.creditcard();
							d.get(url);
							break;
							
						case "CREATEACCOUNT":
							res = ca.createaccount();
							d.get(url);
							break;

						default:
							break;
						}

						String tsres = null;

						if (res)
						{
							tsres = "Pass";
							XLUtils.setCellData(xlfile, tssheet, j, 3, tsres);
							XLUtils.fillGreenColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							XLUtils.setCellData(xlfile, tcsheet, i, 3, tcres);
							XLUtils.fillGreenColor(xlfile, tcsheet, i, 3);
						} 
						else 
						{
							tsres = "Fail";
							XLUtils.setCellData(xlfile, tssheet, j, 3, tsres);
							XLUtils.fillRedColor(xlfile, tssheet, j, 3);
							tcres = tsres;
							XLUtils.setCellData(xlfile, tcsheet, i, 3, tcres);
							XLUtils.fillRedColor(xlfile, tcsheet, i, 3);
						}
					}
				}
			} 
			else 
			{
				XLUtils.setCellData(xlfile, tcsheet, i, 3, "Not Executed");
				XLUtils.fillOrangeColor(xlfile, tcsheet, i, 3);
				
			}
		}
	}
	
	private void saveInputParameters(String username, String password, String clientname) {
		if(username != null){
			InputParameters.username = username;
		}
		if(password != null){
			InputParameters.password = password;
		}	
		if(clientname != null){
			InputParameters.clientname = clientname;
		}
	}

}
